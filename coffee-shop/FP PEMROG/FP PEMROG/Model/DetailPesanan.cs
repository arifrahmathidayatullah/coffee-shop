using System;
using System.Collections.Generic;
using System.Text;

namespace FP_PEMROG.Model
{
    public class DetailPesanan
    {
        public int TotalPesanan { get; set; }
        public int HargaTotal { get; set; }
        public string NamaPesanan { get; set; }
        public int TabelNumber { get; set; }
        public string NamaPembeli { get; set; }
    }
}
