using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FP_PEMROG.View
{
    /// <summary>
    /// Interaction logic for DigitalMoney.xaml
    /// </summary>
    public partial class DigitalMoney : Window
    {
        public DigitalMoney()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new Pembayaran().Show();
            this.Close();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            new OVO_GOPAY_DANA().Show();
            this.Close();
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            new OVO_GOPAY_DANA().Show();
            this.Close();
        }

        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            new OVO_GOPAY_DANA().Show();
            this.Close();
        }

        
    }
}
