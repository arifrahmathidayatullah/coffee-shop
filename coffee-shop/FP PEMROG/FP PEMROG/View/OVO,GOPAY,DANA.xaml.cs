using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FP_PEMROG.View
{
    /// <summary>
    /// Interaction logic for OVO_GOPAY_DANA.xaml
    /// </summary>
    public partial class OVO_GOPAY_DANA : Window
    {
        public OVO_GOPAY_DANA()
        {
            InitializeComponent();
        }

        private void Button_Back(object sender, RoutedEventArgs e)
        {
            new TampilanAkhir().Show();
            this.Close();
        }
    }
}
