using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FP_PEMROG.View
{
    /// <summary>
    /// Interaction logic for Coffee.xaml
    /// </summary>
    public partial class Coffee : Window
    {
        public Coffee()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new Pembayaran().Show();
            this.Close();
        }

        private void Button_Back(object sender, RoutedEventArgs e)
        {
            new Menu().Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (cb1.Text == "1")
            {
                biaya1.Text = "" + 5000 * 1;
            }
            if (cb1.Text == "2")
            {
                biaya1.Text = "" + 5000 * 2;
            }
            if (cb1.Text == "3")
            {
                biaya1.Text = "" + 5000 * 3;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (cb2.Text == "1")
            {
                biaya2.Text = "" + 15000 * 1;
            }
            if (cb2.Text == "2")
            {
                biaya2.Text = "" + 15000 * 2;
            }
            if (cb2.Text == "3")
            {
                biaya2.Text = "" + 15000 * 3;
            }

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (cb3.Text == "1")
            {
                biaya3.Text = "" + 7000 * 1;
            }
            if (cb3.Text == "2")
            {
                biaya3.Text = "" + 7000 * 2;
            }
            if (cb3.Text == "3")
            {
                biaya3.Text = "" + 7000 * 3;
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (cb4.Text == "1")
            {
                biaya4.Text = "" + 10000 * 1;
            }
            if (cb4.Text == "2")
            {
                biaya4.Text = "" + 10000 * 2;
            }
            if (cb4.Text == "3")
            {
                biaya4.Text = "" + 10000 * 3;
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            if (cb5.Text == "1")
            {
                biaya5.Text = "" + 2000 * 1;
            }
            if (cb5.Text == "2")
            {
                biaya5.Text = "" + 2000 * 2;
            }
            if (cb5.Text == "3")
            {
                biaya5.Text = "" + 2000 * 3;
            }
        }
    }
}
