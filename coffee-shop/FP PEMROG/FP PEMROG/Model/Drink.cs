using System;
using System.Collections.Generic;
using System.Text;

namespace FP_PEMROG.Model
{
    public class Drink
    {
        public string JenisDrink { get; set; }
        public int HargaDrink { get; set; }
    }
}